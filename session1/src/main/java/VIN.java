import java.util.regex.Pattern;

class VIN {
    static class VDS {
        public String AutoCharacters;
        public int ControlNumber;
    }

    static class VIS {
        public char ModelYear;
        public String PlantCode;
        public String SerialNumber;
    }

    public static final Pattern PATTERN = Pattern.compile("[ABCDEFGHJKLMNPRSTUVWXYZ1234567890]{17}", Pattern.CASE_INSENSITIVE);
    public String origin;
    public String WMI;
    public VDS VDS;
    public VIS VIS;

    public static VIN parse(String vincode) {
        if (!PATTERN.matcher(vincode).matches()) {
            return null;
        }

        VIN vin = new VIN();
        vin.origin = vincode;
        vin.WMI = vincode.substring(0, 3);

        VDS vds = new VDS();
        vds.AutoCharacters = vincode.substring(3, 8);

        String chk = vincode.substring(8, 9);
        vds.ControlNumber = chk.equals("X") ? 10 : Integer.valueOf(chk);
        vin.VDS = vds;

        VIS vis = new VIS();
        vis.ModelYear = vincode.charAt(9);
        vis.PlantCode = vincode.substring(10, 11);
        vis.SerialNumber = vincode.substring(11);
        vin.VIS = vis;

        return vin;
    }

    public boolean CheckSum() {
        if (VDS.ControlNumber > 9 || VDS.ControlNumber < 0) {
            return false;
        }

        double hash = 0;
        for (int i = 0; i < 17; i++) {
            int code = numberCode(i);
            if (code == -1) {
                continue;
            }

            hash += code * weight(i);
        }

        int finalHash = (int)(Math.floor(hash / 11.0) * 11);

        return Math.abs(hash - finalHash) == VDS.ControlNumber;
    }

    private int weight(int index) {
        if (index == 8) {
            return -1;
        }
        if (index == 7) {
            return 10;
        }
        if (index < 7) {
            return 8 - index;
        }
        return 18 - index;
    }

    private int numberCode(int index) {
        int[] codes = { 1, 8, 4, 3, 4, 5, 6, 5, -1, 7, 3, 4, 0, 4, 4, 5, 3 };
        return codes[index];
    }
}
