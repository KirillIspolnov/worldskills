public class VINChecker {
    public static void main(String... args) {
    }

    public static boolean CheckVIN(String code) {
        VIN vin = VIN.parse(code);
        if (vin == null) {
            return false;
        }

        return vin.CheckSum();
    }

    public String GetVINCountry(String code) {
        VIN vin = VIN.parse(code);
        if (vin == null) {
            return "";
        }

        return CountryCodes.get(vin.WMI.substring(0, 2));
    }

    public Integer GetTransportYear(String code) {
        VIN vin = VIN.parse(code);
        if (vin == null) {
            return 0;
        }

        if (vin.VIS.ModelYear >= '0' && vin.VIS.ModelYear <= '9') {
            return 2000 + Integer.valueOf(vin.VIS.ModelYear);
        }

        int delta = vin.VIS.ModelYear - 'A';
        return 1980 + delta;
    }
}
