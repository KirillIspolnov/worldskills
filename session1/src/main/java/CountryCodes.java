import java.util.HashMap;



public class CountryCodes {
    private static HashMap<String, String> map;

    static {
        for (char i = 'A'; i <= 'H'; i++) map.put("A" + i, "ЮАР");
        for (char i = 'J'; i <= 'N'; i++) map.put("A" + i, "Кот-д'ивуар");
        for (char i = 'A'; i <= 'E'; i++) map.put("B" + i, "Ангола");
        for (char i = 'F'; i <= 'K'; i++) map.put("B" + i, "Кения");
        for (char i = 'L'; i <= 'R'; i++) map.put("B" + i, "Танзания");
        for (char i = 'A'; i <= 'E'; i++) map.put("C" + i, "Бенин");
        for (char i = 'F'; i <= 'K'; i++) map.put("C" + i, "Мадагаскар");
        for (char i = 'L'; i <= 'R'; i++) map.put("C" + i, "Тунис");
        for (char i = 'A'; i <= 'E'; i++) map.put("D" + i, "Египет");
        for (char i = 'F'; i <= 'K'; i++) map.put("D" + i, "Марокко");
        for (char i = 'L'; i <= 'R'; i++) map.put("D" + i, "Замбия");
        for (char i = 'A'; i <= 'E'; i++) map.put("E" + i, "Эфиопия");
        for (char i = 'F'; i <= 'K'; i++) map.put("E" + i, "Мозамбик");
        for (char i = 'A'; i <= 'E'; i++) map.put("F" + i, "Гана");
        for (char i = 'F'; i <= 'K'; i++) map.put("F" + i, "Нигерия");
        for (char i = 'A'; i <= 'T'; i++) map.put("J" + i, "Япония");
        for (char i = 'A'; i <= 'E'; i++) map.put("K" + i, "Шри Ланка");
        for (char i = 'F'; i <= 'K'; i++) map.put("K" + i, "Израиль");
        for (char i = 'L'; i <= 'R'; i++) map.put("K" + i, "Южная Корея");
        for (char i = 'S'; i <= 'Z'; i++) map.put("K" + i, "Казахстан");
            map.put("K0", "Казахстан");
        for (char i = 'A'; i <= 'Z'; i++) map.put("L" + i, "Китай");
            map.put("L0", "Китай");
        for (char i = 'A'; i <= 'E'; i++) map.put("M" + i, "Индия");
        for (char i = 'F'; i <= 'K'; i++) map.put("M" + i, "Индонезия");
        for (char i = 'L'; i <= 'R'; i++) map.put("M" + i, "Таиланд");
        for (char i = 'F'; i <= 'K'; i++) map.put("N" + i, "Пакистан");
        for (char i = 'L'; i <= 'R'; i++) map.put("N" + i, "Турция");
        for (char i = 'A'; i <= 'E'; i++) map.put("P" + i, "Филиппины");
        for (char i = 'F'; i <= 'K'; i++) map.put("P" + i, "Сингапур");
        for (char i = 'L'; i <= 'R'; i++) map.put("P" + i, "Малайзия");
        for (char i = 'A'; i <= 'E'; i++) map.put("R" + i, "ОАЭ");
        for (char i = 'F'; i <= 'K'; i++) map.put("R" + i, "Тайвань");
        for (char i = 'L'; i <= 'R'; i++) map.put("R" + i, "Вьетнам");
        for (char i = 'S'; i <= 'Z'; i++) map.put("R" + i, "Саудовская Аравия");
            map.put("R0", "Саудовская Аравия");
        for (char i = 'A'; i <= 'M'; i++) map.put("S" + i, "Великобритания");
        for (char i = 'N'; i <= 'T'; i++) map.put("S" + i, "Германия");
        for (char i = 'U'; i <= 'Z'; i++) map.put("S" + i, "Польша");
        for (char i = '1'; i <= '4'; i++) map.put("S" + i, "Латвия");
        for (char i = 'A'; i <= 'H'; i++) map.put("T" + i, "Швейцария");
        for (char i = 'J'; i <= 'P'; i++) map.put("T" + i, "Чехия");
        for (char i = 'R'; i <= 'V'; i++) map.put("T" + i, "Венгрия");
        for (char i = 'W'; i <= 'Z'; i++) map.put("T" + i, "Португалия");
            for (char i = '0'; i <= '1'; i++) map.put("T" + i, "Португалия");
        for (char i = 'H'; i <= 'M'; i++) map.put("U" + i, "Дания");
        for (char i = 'N'; i <= 'T'; i++) map.put("U" + i, "Ирландия");
        for (char i = 'U'; i <= 'Z'; i++) map.put("U" + i, "Румыния");
        for (char i = '5'; i <= '7'; i++) map.put("U" + i, "Слования");
        for (char i = 'A'; i <= 'E'; i++) map.put("V" + i, "Австрия");
        for (char i = 'F'; i <= 'R'; i++) map.put("V" + i, "Франция");
        for (char i = 'S'; i <= 'W'; i++) map.put("V" + i, "Испания");
        for (char i = 'X'; i <= 'Z'; i++) map.put("V" + i, "Сербия");
            for (char i = '0'; i <= '2'; i++) map.put("V" + i, "Сербия");
        for (char i = '3'; i <= '5'; i++) map.put("V" + i, "Хорватия");
        for (char i = '6'; i <= '9'; i++) map.put("V" + i, "Эстония");

    }


    public static String get(String code) {
        return map.get(code);
    }
}
