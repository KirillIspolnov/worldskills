public class PlateSerial {
    public char[] serial;
    public int number;

    public static PlateSerial parse(String mark) {
        char[] serial = { mark.charAt(0), mark.charAt(4), mark.charAt(5) };
        int number = Integer.valueOf(mark.substring(1, 4));

        PlateSerial plate = new PlateSerial();
        plate.serial = serial;
        plate.number = number;

        return plate;
    }
}
