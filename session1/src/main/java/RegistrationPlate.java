import java.util.regex.Pattern;

public class RegistrationPlate {
    private static final String ALPHABET = "ABEKMHOPCTYX";
    private static final Pattern PLATE_PATTERN = Pattern.compile("(ABEKMHOPCTYУX)\\d{3}(ABEKMHOPCTYУX){2}\\d{3}", Pattern.CASE_INSENSITIVE);

    public static void main(String... args) {
        System.out.println(GetNextMarkAfter("A999AX"));
    }

    public static boolean CheckMark(String mark) {
        if (!PLATE_PATTERN.matcher(mark).matches()) {
            return false;
        }

        int code = Integer.valueOf(mark.substring(6));
        return RegionCodes.exists(code);
    }

    private static String fillZeros(int number) {
        if (number == 0) {
            return "000";
        }
        if (number < 10) {
            return String.format("00%d", number);
        }
        if (number < 100) {
            return String.format("0%d", number);
        }
        return String.valueOf(number);
    }

    public static String GetNextMarkAfter(String markII) {
        PlateSerial plate = PlateSerial.parse(markII);

        if (plate.number < 999) {
            return plate.serial[0] + fillZeros(plate.number + 1) + plate.serial[1] + plate.serial[2];
        }

        int lastIndex = ALPHABET.indexOf(plate.serial[2]) + 1;
        int middleIndex = ALPHABET.indexOf(plate.serial[1]);
        int startIndex = ALPHABET.indexOf(plate.serial[0]);

        if (lastIndex >= ALPHABET.length()) {
            middleIndex++;
            lastIndex = 0;
        }

        if (middleIndex >= ALPHABET.length()) {
            startIndex++;
            middleIndex = 0;
        }

        if (startIndex >= ALPHABET.length()) {
            return "out of stock";
        }

        return ALPHABET.charAt(startIndex) + "001" + ALPHABET.charAt(middleIndex) + ALPHABET.charAt(lastIndex);
    }

    public static String GetNextMarkAfterInRange(String mark, String rangeStart, String rangeEnd) {
        PlateSerial plate = PlateSerial.parse(mark);

        if (plate.number < 999) {
            PlateSerial start = PlateSerial.parse(rangeStart);
            if (plate.number < start.number) {
                plate.number = start.number;
            }
            return plate.serial[0] + fillZeros(plate.number + 1) + plate.serial[1] + plate.serial[2];
        }

        int lastIndex = ALPHABET.indexOf(plate.serial[2]) + 1;
        int middleIndex = ALPHABET.indexOf(plate.serial[1]);
        int startIndex = ALPHABET.indexOf(plate.serial[0]);

        if (lastIndex >= ALPHABET.length()) {
            middleIndex++;
            lastIndex = 0;
        }

        if (middleIndex >= ALPHABET.length()) {
            startIndex++;
            middleIndex = 0;
        }

        if (startIndex >= ALPHABET.length()) {
            return "out of stock";
        }

        return ALPHABET.charAt(startIndex) + "001" + ALPHABET.charAt(middleIndex) + ALPHABET.charAt(lastIndex);
    }
}
