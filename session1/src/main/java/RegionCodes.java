import java.util.HashMap;

public class RegionCodes {
    private static final HashMap<Integer, String> codes = new HashMap<>();

    static {
        codes.put(1, "Республика Адыгея");
        codes.put(2, "Республика Башкортостан");
        codes.put(102, "Республика Башкортостан");
        codes.put(3, "Республика Бурятия");
        codes.put(4, "Республика Алтай");
        codes.put(5, "Республика Дагестан");
    }

    public static boolean exists(int code) {
        return codes.containsKey(code);
    }

    public static String get(int code) {
        return codes.get(code);
    }
}
