package org.ws.models;

import org.ws.models.driver.*;

import java.util.UUID;

public class Driver {
    private UUID id;
    private String surname;
    private String name;
    private String patronymic;
    private DriverContacts contacts;
    private DriverPassport passport;
    private Photo photo;
    private DriverAddress address;
    private DriverJob job;
    private String notes;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public DriverContacts getContacts() {
        return contacts;
    }

    public void setContacts(DriverContacts contacts) {
        this.contacts = contacts;
    }

    public DriverPassport getPassport() {
        return passport;
    }

    public void setPassport(DriverPassport passport) {
        this.passport = passport;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public DriverAddress getAddress() {
        return address;
    }

    public void setAddress(DriverAddress address) {
        this.address = address;
    }

    public DriverJob getJob() {
        return job;
    }

    public void setJob(DriverJob job) {
        this.job = job;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
