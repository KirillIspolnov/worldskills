package org.ws.models.driver;

import java.util.UUID;

public class DriverAddress {
    private UUID registrationId;
    private UUID liveId;
    private String registrationCity;
    private String registrationPlace;
    private String liveCity;
    private String livePlace;

    public UUID getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(UUID id) {
        this.registrationId = id;
    }
    
    public UUID getLiveId() {
        return liveId;
    }

    public void setLiveId(UUID liveId) {
        this.liveId = liveId;
    }

    public String getRegistrationCity() {
        return registrationCity;
    }

    public void setRegistrationCity(String registrationCity) {
        this.registrationCity = registrationCity;
    }

    public String getRegistrationPlace() {
        return registrationPlace;
    }

    public void setRegistrationPlace(String registrationPlace) {
        this.registrationPlace = registrationPlace;
    }

    public String getLiveCity() {
        return liveCity;
    }

    public void setLiveCity(String liveCity) {
        this.liveCity = liveCity;
    }

    public String getLivePlace() {
        return livePlace;
    }

    public void setLivePlace(String livePlace) {
        this.livePlace = livePlace;
    }
}
