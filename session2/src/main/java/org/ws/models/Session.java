package org.ws.models;

import java.util.Date;
import java.util.UUID;

public class Session {
    public static enum Status {
        FAILED, BLOCKED, SUCCESS
    }

    private UUID id;
    private User user;
    private Date date;
    private Status status;
    
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
