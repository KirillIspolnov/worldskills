package org.ws.abstraction;

public interface LoginService {
    public static enum SystemState {
        AUTHORIZED,
        DEFAULT,
        LOCKDOWN
    }
    
    public SystemState login(String login, String password);
    public long lockdownTimeout();
}
