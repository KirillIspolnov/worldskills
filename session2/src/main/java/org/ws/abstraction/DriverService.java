package org.ws.abstraction;

import java.io.File;
import java.util.List;
import java.util.UUID;

import org.ws.models.Driver;
import org.ws.models.Photo;

public interface DriverService {
    public static enum PhotoErrors {
        TYPE,
        SIZE,
        WEIGHT,
        HORIZONTAL
    }
    
    public PhotoErrors checkPhoto(File file);
    
    public void updatePhotoFromFile(Photo photo, UUID driverId, File file);
    
    public boolean checkEmail(String email);
    
    public List<Driver> allDrivers();
    
    public UUID saveDriver(Driver driver);
}
