package org.ws.abstraction;

import java.util.List;
import java.util.UUID;

import org.ws.models.Driver;

public interface DriversRepository {
    public void init() throws Exception;
    public List<Driver> list() throws Exception;
    public Driver get(UUID id) throws Exception;
    public void save(Driver driver) throws Exception;
}
