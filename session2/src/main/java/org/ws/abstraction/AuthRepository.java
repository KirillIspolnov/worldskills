package org.ws.abstraction;

import org.ws.models.Session;
import org.ws.models.User;

public interface AuthRepository {
    public void init() throws Exception;
    public User getUser(String login) throws Exception;
    public Session getUserSession(User user) throws Exception;
    public void saveSession(Session session) throws Exception;
}
