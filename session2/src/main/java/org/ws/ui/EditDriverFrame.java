package org.ws.ui;

import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.ws.abstraction.DriverService;
import org.ws.models.Driver;

public class EditDriverFrame {
    private JFrame frame;
    private JPanel panel;
    private CustomTextField idField;
    private CustomTextField nameField;
    private CustomTextField surnameField;
    private CustomTextField patronymicField;
    private CustomTextField phoneField;
    private CustomTextField emailField;
    private CustomTextField passportField;
    private CustomTextField registrationCityField;
    private CustomTextField registrationPlaceField;
    private CustomTextField liveCityField;
    private CustomTextField livePlaceField;
    private CustomTextField jobPlaceField;
    private CustomTextField jobPostField;
    private CustomTextField photoPathField;
    private JButton chooseFile;
    private JFileChooser fileChooser;
    private File photo;
    
    private JButton save;
    
    public EditDriverFrame(DriverService service, Driver driver) {
        frame = new JFrame(driver.getId().toString());
        
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setContentPane(panel);
        
        idField = new CustomTextField(driver.getId().toString());
        idField.setEditable(false);
        panel.add(idField);
        
        nameField = new CustomTextField("Имя");
        nameField.setText(driver.getName());
        panel.add(nameField);
        
        surnameField = new CustomTextField("Фамилия");
        surnameField.setText(driver.getSurname());
        panel.add(surnameField);

        patronymicField = new CustomTextField("Отчество");
        patronymicField.setText(driver.getPatronymic());
        panel.add(patronymicField);

        phoneField = new CustomTextField("Телефон");
        phoneField.setText(driver.getContacts().getPhone());
        panel.add(phoneField);

        emailField = new CustomTextField("Почта");
        emailField.setText(driver.getContacts().getEmail());
        panel.add(emailField);        

        passportField = new CustomTextField("Паспорт");
        passportField.setText(driver.getPassport().getSeries() + " " + driver.getPassport().getNumber());
        panel.add(passportField);

        registrationCityField = new CustomTextField("Город регистрации");
        registrationCityField.setText(driver.getAddress().getRegistrationCity());
        panel.add(registrationCityField);

        registrationPlaceField = new CustomTextField("Адрес регистрации");
        registrationPlaceField.setText(driver.getAddress().getRegistrationPlace());
        panel.add(registrationPlaceField);
        
        liveCityField = new CustomTextField("Город проживания");
        liveCityField.setText(driver.getAddress().getLiveCity());
        panel.add(liveCityField);

        livePlaceField = new CustomTextField("Адрес проживания");
        livePlaceField.setText(driver.getAddress().getLivePlace());
        panel.add(livePlaceField);

        jobPlaceField = new CustomTextField("Место работы");
        jobPlaceField.setText(driver.getJob().getPlace());
        panel.add(jobPlaceField);
        
        jobPostField = new CustomTextField("Должность");
        jobPostField.setText(driver.getJob().getPost());
        panel.add(jobPostField);
        
        photoPathField = new CustomTextField("Путь к фото");
        photoPathField.setText(driver.getPhoto().getId().toString());
        panel.add(photoPathField);
        
        chooseFile = new JButton("Выбрать фото");
        chooseFile.addActionListener(e -> {
            int result = fileChooser.showOpenDialog(frame);
            if (result == JFileChooser.APPROVE_OPTION) {
                photo = fileChooser.getSelectedFile();
                
                var validResult = service.checkPhoto(photo);
                if (validResult == null) {
                    photoPathField.setText(photo.getPath());
                    return;
                }
                
                switch(validResult) {
                    case WEIGHT: 
                        JOptionPane.showMessageDialog(frame, "Размер файла фото не должен превышать 2 МБ"); 
                        break;
                    case SIZE: 
                        JOptionPane.showMessageDialog(frame, "Требуемый размер фото - 3х4"); 
                        break;
                    case TYPE: 
                        JOptionPane.showMessageDialog(frame, "Поддерживаются только PNG и JPG"); 
                        break;
                    case HORIZONTAL: 
                        JOptionPane.showMessageDialog(frame, "Ориентация фото должна быть вертикальной"); 
                        break;
                }
                
                photo = null;                
            }
        });
        panel.add(chooseFile);
        
        fileChooser = new JFileChooser();
        
        save = new JButton("Сохранить");
        save.addActionListener(e -> {
            if (!service.checkEmail(emailField.getText())) {
                JOptionPane.showMessageDialog(frame, "Адрес электронной почты не соответствует формату username@site.domain. Примеры правильных адресов: admin@reg.ru, inspetor@gov.ru, manager@kek.lol");
                return;
            }
            
            driver.setName(nameField.getText());
            driver.setSurname(surnameField.getText());
            driver.setPatronymic(patronymicField.getText());
            
            var contacts = driver.getContacts();
            contacts.setPhone(phoneField.getText());
            contacts.setEmail(emailField.getText());
            
            var passport = driver.getPassport();
            var passportSeries = passportField.getText().substring(0, 4);
            passport.setSeries(Integer.valueOf(passportSeries));
            var passportNumber  = passportField.getText().substring(4).trim();
            passport.setNumber(Integer.valueOf(passportNumber));
            
            var photo = driver.getPhoto();
            if (!photoPathField.getText().equals(photo.getId().toString())) {
                service.updatePhotoFromFile(photo, driver.getId(), this.photo);
            }
            
            var address = driver.getAddress();
            address.setRegistrationCity(registrationCityField.getText());
            address.setRegistrationPlace(registrationPlaceField.getText());
            address.setLiveCity(liveCityField.getText());
            address.setLivePlace(livePlaceField.getText());
            
            var job = driver.getJob();
            job.setPlace(jobPlaceField.getText());
            job.setPost(jobPostField.getText());
            
            var id = service.saveDriver(driver);
            if (id != null) {
                JOptionPane.showMessageDialog(frame, "Изменения сохранены. ID водителя: " + id);
            }
        });
    }
    
    public void show() {
        frame.setVisible(true);
    }
}
