package org.ws.ui;

import java.util.List;

import javax.swing.JButton;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.ws.abstraction.DriverService;
import org.ws.models.Driver;

public class DriverTableModel implements TableModel {
    private DriverService service;
    private List<Driver> drivers;
    
    public DriverTableModel(DriverService service) {
        this.service = service;
        this.drivers = service.allDrivers();
    }
    
    @Override
    public int getRowCount() {
        return drivers.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return switch(columnIndex) {
            case 0 -> "ID";
            case 1 -> "Имя";
            case 2 -> "Фамилия";
            case 3 -> "Отчество";
            case 4 -> "";
            default -> "";
        };
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 4) {
            return JButton.class;
        }
        
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        final Driver driver = drivers.get(rowIndex);
        return switch(columnIndex) {
            case 0 -> driver.getId().toString();
            case 1 -> driver.getName();
            case 2 -> driver.getSurname();
            case 3 -> driver.getPatronymic();
            case 4 -> {
                var button = new JButton("Редактировать");
                button.addActionListener(e -> openEditFrame(driver));
                yield button;
            }
            default -> null;
        };
    }
    
    private void openEditFrame(Driver driver) {
        var editFrame = new EditDriverFrame(service, driver);
        editFrame.show();
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        
    }

}
