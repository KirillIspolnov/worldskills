package org.ws.ui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;

import org.ws.abstraction.LoginService;
import org.ws.abstraction.LoginService.SystemState;
import org.ws.utils.UI;

public class LoginFrame {
    private AtomicInteger attemptCount = new AtomicInteger(0);
    private AtomicBoolean lockdown = new AtomicBoolean(false);
    private SimpleDateFormat lockdownFormat = new SimpleDateFormat("mm:ss");
    private JFrame frame;
    private JPanel panel;
    private JLabel title;
    private JTextField loginField;
    private JPasswordField passwordField;
    private JButton loginButton;
    private JLabel lockdownTimer;
    
    public LoginFrame(LoginService service, FrameHook frameHook) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e) {
            System.err.println(e);
        }
        
        frame = new JFrame("Login");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        title = new JLabel("Авторизация");
        title.setFont(title.getFont().deriveFont(28f));
        panel.add(title);
        
        loginField = new CustomTextField("Логин");
        panel.add(loginField);
        
        passwordField = new JPasswordField();
        panel.add(passwordField);
        
        loginButton = new JButton("Войти");
        loginButton.addActionListener((e) -> {
            if (lockdown.get()) {
                return;
            }

            attemptCount.incrementAndGet();
            lockdownTimer.setText("");
            var result = service.login(loginField.getText(), new String(passwordField.getPassword()));
            
            if (result == SystemState.LOCKDOWN) {
                lockdown.set(true);
                var thread = new Thread(() -> {
                   long timeout = service.lockdownTimeout();
                   
                   while(timeout > 0) {
                       var date = new Date(timeout);
                       
                       UI.invokeTask(() ->
                           lockdownTimer.setText("Система заблокирована на " + lockdownFormat.format(date)));
                      
                       timeout -= 1000;
                       try {
                           Thread.sleep(1000L);
                       } catch (InterruptedException e1) {
                           e1.printStackTrace();
                       }
                   }

                   lockdownTimer.setText("");
                   lockdown.set(false);
                });
                thread.start();
                return;
            } else if (result == SystemState.DEFAULT) {
                lockdownTimer.setText("Пользователь не найден");
                final int attemptId = attemptCount.get();
                
                UI.invokeTask(() -> {
                    if (attemptCount.get() == attemptId) {
                        lockdownTimer.setText(""); 
                    }
                }, 5000L);
            }
            
            frameHook.showDriversListFrame();
        });
        panel.add(loginButton);
        
        lockdownTimer = new JLabel(" ");
        panel.add(lockdownTimer);        
        
        frame.setContentPane(panel);        
    }
    
    public void show() {
        frame.pack();
        frame.setVisible(true);
    }
}
