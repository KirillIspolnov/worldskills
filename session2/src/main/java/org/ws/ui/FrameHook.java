package org.ws.ui;

public interface FrameHook {
    public void showLoginFrame();
    public void showDriversListFrame();
}
