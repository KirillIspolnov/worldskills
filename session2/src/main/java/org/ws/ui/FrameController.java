package org.ws.ui;

import org.ws.abstraction.DriverService;
import org.ws.abstraction.LoginService;

public class FrameController implements FrameHook {
    private LoginFrame loginFrame;
    private DriversListFrame listFrame;
    
    public FrameController(LoginService loginService, DriverService driverService) {        
        this.loginFrame = new LoginFrame(loginService, this);
        this.listFrame = new DriversListFrame(driverService, this);
    }

    @Override
    public void showLoginFrame() {
        loginFrame.show();
    }

    @Override
    public void showDriversListFrame() {
        listFrame.show();
    }
}
