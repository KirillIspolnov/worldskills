package org.ws.ui;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;

import org.ws.abstraction.DriverService;

public class DriversListFrame {
    private JFrame frame;
    private JPanel panel;
    private JTable table;
    
    public DriversListFrame(DriverService service, FrameHook frameHook) {        
        frame = new JFrame("Список водителей");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        frame.setContentPane(panel);
        
        table = new JTable(new DriverTableModel(service));
        panel.add(table);
    }
    
    public void show() {
        frame.setVisible(true);
    }
}
