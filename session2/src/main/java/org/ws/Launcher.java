package org.ws;

import org.ws.service.DriverServiceImpl;
import org.ws.service.LoginServiceImpl;
import org.ws.sql.SqlAuthRepository;
import org.ws.sql.SqlDriversRepository;
import org.ws.ui.FrameController;

public class Launcher {
    public static void main(String... args) {
        //final var URL = "jdbc:postgresql://127.0.0.1:5432/worlskills?user=worldskills&password=worldskills&ssl=false";
        final var URL = "jdbc:postgresql://192.168.1.244:5432/User8?user=User8&password=User8&ssl=false";
        
        var authRepository = new SqlAuthRepository(URL);
        var driverRepository = new SqlDriversRepository(URL);
        
        var loginService = new LoginServiceImpl(authRepository);
        var driverService = new DriverServiceImpl(driverRepository);
        
        FrameController controller = new FrameController(loginService, driverService);
        controller.showLoginFrame();
    }
}
