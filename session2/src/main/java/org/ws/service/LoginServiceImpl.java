package org.ws.service;

import java.security.MessageDigest;

import org.ws.abstraction.AuthRepository;
import org.ws.abstraction.LoginService;
import org.ws.models.Session;
import org.ws.models.Session.Status;
import org.ws.models.User;

public class LoginServiceImpl implements LoginService {
    private static final MessageDigest SHA256; 
    static {
        try {
            SHA256 = MessageDigest.getInstance("SHA-256");
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private AuthRepository authRepository;
    
    public LoginServiceImpl(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }
    
    @Override
    public SystemState login(String login, String password) {
        User user;
        try {
            user = authRepository.getUser(login);            
        } catch(Exception e) {
            return SystemState.LOCKDOWN;
        }
        
        Session session;
        try {
            session = authRepository.getUserSession(user);
        } catch(Exception e) {
            return SystemState.LOCKDOWN;
        }
        
        if (session == null) {
            session = new Session();
        } else if (session.getStatus() == Status.BLOCKED){
            return SystemState.LOCKDOWN;
        }
        
        var state = SystemState.DEFAULT;
        
        if (user.getPasswordHash() == hash(password)) {
            session.setStatus(Status.SUCCESS);
            state = SystemState.AUTHORIZED;
        } else {
            session.setStatus(Status.FAILED);
        }

        try {
            authRepository.saveSession(session);
        } catch(Exception e) {
            return SystemState.LOCKDOWN;
        }
                
        return state;
    }

    @Override
    public long lockdownTimeout() {
        return 3000L;
    }
    
    private String hash(String password) {
        var bytes = SHA256.digest(password.getBytes());
        for (int i = 0; i < bytes.length; i++) {
            if (!Character.isAlphabetic(bytes[i])) {
                bytes[i] = '#';
            }
        }
        return new String(bytes);
    }

}
