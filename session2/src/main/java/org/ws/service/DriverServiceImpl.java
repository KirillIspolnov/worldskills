package org.ws.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.ws.abstraction.DriverService;
import org.ws.abstraction.DriversRepository;
import org.ws.models.Driver;
import org.ws.models.Photo;

public class DriverServiceImpl implements DriverService {
    private DriversRepository repository;
    
    public DriverServiceImpl(DriversRepository repository) {
        this.repository = repository;
    }

    @Override
    public PhotoErrors checkPhoto(File file) {
        return null;
    }

    @Override
    public boolean checkEmail(String email) {
        return email.matches("[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z]{2,}");
    }

    @Override
    public List<Driver> allDrivers() {
        try {
            return repository.list();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @Override
    public UUID saveDriver(Driver driver) {
        try {
            repository.save(driver);
        } catch (Exception e) {
            e.printStackTrace();
            return driver.getId();
        }
        return null;
    }

    @Override
    public void updatePhotoFromFile(Photo photo, UUID driverId, File file) {
        try(var inputStream = new FileInputStream(file);
            var bufferedStream = new BufferedInputStream(inputStream)) {
            
            var byteStream = new ByteArrayOutputStream();
            byte[] packet = new byte[4096];
            int length;
            
            while((length = bufferedStream.read(packet)) > 0) {
                byteStream.write(packet, 0, length);
            }
            
            photo.setImage(byteStream.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

}
