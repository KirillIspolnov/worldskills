package org.ws.sql;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ws.abstraction.DriversRepository;
import org.ws.models.Driver;
import org.ws.models.Photo;
import org.ws.models.driver.DriverAddress;
import org.ws.models.driver.DriverContacts;
import org.ws.models.driver.DriverJob;
import org.ws.models.driver.DriverPassport;
import org.ws.utils.ID;

public class SqlDriversRepository extends SqlDatabase implements DriversRepository {

    public SqlDriversRepository(String url) {
        super(url);
    }

    @Override
    public void init() {
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
            execute("CREATE TABLE IF NOT EXISTS drivers (id uuid PRIMARY KEY, name text, surname text, patronymic text)");
            execute("CREATE TABLE IF NOT EXISTS drivers_passport (id uuid PRIMARY KEY, driver uuid REFERENCES drivers(id), series int, number int)");
            execute("CREATE TABLE IF NOT EXISTS drivers_address (id uuid PRIMARY KEY, driver uuid REFERENCES drivers(id), city text, place text, mode int)");
            execute("CREATE TABLE IF NOT EXISTS drivers_contacts (id uuid PRIMARY KEY, driver uuid REFERENCES drivers(id), phone text, email text)");
            execute("CREATE TABLE IF NOT EXISTS drivers_job (id uuid PRIMARY KEY, driver uuid REFERENCES drivers(id), place text, post text)");
            execute("CREATE TABLE IF NOT EXISTS drivers_photo (id uuid PRIMARY KEY, driver uuid REFERENCES drivers(id), path text)");
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    private static final String SELECT_ALL = """
            SELECT
            d.id as driverId,
            d.name as driverName,
            d.surname as driverSurname,
            d.patronymic as driverPatronymic,
            dps.id as passportId,
            dps.number as passportNumber,
            dps.series as passportSeries,
            dc.id as contactId,
            dc.phone as contactPhone,
            dc.email as contactEmail,
            dar.id as registrationId,
            dar.city as registrationCity,
            dar.place as registrationPlace,
            dal.id as liveId,
            dal.city as liveCity,
            dal.place as livePlace,
            dj.id as jobId,
            dj.place as jobPlace,
            dj.post as jobPost,
            dph.id as photoId,
            dph.path ad photoPath
            FROM drivers as d
            JOIN drivers_passport AS dps ON dps.driver = d.id
            JOIN drivers_contacts AS dc ON dc.driver = d.id
            JOIN drivers_address AS dar ON dar.driver = d.id and dar.mode = 0
            JOIN drivers_address AS dal ON dal.driver = d.id and dal.mode = 1
            JOIN drivers_job AS dj ON dj.driver = d.id
            JOIN drivers_photo AS dph ON dph.driver = d.id""";
    
    private static final String SELECT_BY_ID = SELECT_ALL + " WHERE d.id = '%s'";

    private static final String UPDATE_DRIVER = """
            UPDATE drivers SET (
            name='%s',
            surname = '%s', 
            patronymic='%s'
            ) WHERE id='%s'
            """; 
    
    private static final String UPDATE_PASSPORT = """
            UPDATE drivers_passport SET (number=%d, series=%d) WHERE id='%s' 
            """;
    
    private static final String UPDATE_CONTACTS = """
            UPDATE drivers_contacts SET (phone='%s', email='%s') WHERE id='%s'
            """;
    
    private static final String UPDATE_ADDRESS = """
            UPDATE drivers_address SET (city='%s', place='%s') WHERE id='%s'
            """;
    
    private static final String UPDATE_JOB = """
            UPDATE drivers_job SET (place='%s', post='%s') WHERE id='%s'
            """;
    
    private static final String INSERT_DRIVER = """
            INSERT INTO drivers (id, name, surname, patronymic) VALUES ('%s', '%s', '%s', '%s')
            """; 
    
    private static final String INSERT_PASSPORT = """
            INSERT INTO drivers_passport (id, driver, number, series) ('%s', '%s', %d, %d) 
            """;
    
    private static final String INSERT_CONTACTS = """
            INSERT INTO drivers_contacts (id, driver, phone, email) VALUES ('%s', '%s', '%s', '%s')
            """;
    
    private static final String INSERT_ADDRESS = """
            INSERT INTO drivers_address (id, address, mode, city, place) ('%s', '%s', %d, '%s', '%s')
            """;
    
    private static final String INSERT_JOB = """
            INSERT INTO drivers_job (id, driver, place, post) VALUES ('%s', '%s', '%s', '%s')
            """;
    
    private static final String INSERT_PHOTO = """
            INSERT INTO drivers_photo (id, driver, path) VALUES ('%s', '%s', '%s')
            """;
    
    @Override
    public Driver get(UUID id) throws SQLException, IOException {
        try(var connection = getConnection();
            var statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(String.format(SELECT_BY_ID, id.toString()));

            return parseDriver(result);
        }    
    }
    
    @Override
    public List<Driver> list() throws SQLException, IOException {
        try(var connection = getConnection();
            var statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(SELECT_ALL);

            var list = new ArrayList<Driver>();
            Driver driver;
            while((driver = parseDriver(result)) != null) {
                list.add(driver);
            }
            
            return list;
        } 
    }
    
    private Driver parseDriver(ResultSet result) throws SQLException, IOException {
        if (!result.next()) {
            return null;
        }
        
        var driver = new Driver();
        var photo = new Photo();
        
        var photoPath = "";
                
        driver.setId(ID.from(result, "driverId"));
        driver.setName(result.getString("driverName"));
        driver.setSurname(result.getString("driverSurname"));
        driver.setPatronymic(result.getString("driverPatronymic"));

        var passport = new DriverPassport();
        passport.setId(ID.from(result, "passportId"));
        passport.setSeries(result.getInt("passportSeries"));
        passport.setNumber(result.getInt("passportNumber"));
        driver.setPassport(passport);

        var contacts = new DriverContacts();
        contacts.setId(ID.from(result, "contactId"));
        contacts.setPhone(result.getString("contactPhone"));
        contacts.setEmail(result.getString("contactEmail"));
        driver.setContacts(contacts);

        var address = new DriverAddress();
        address.setRegistrationId(ID.from(result, "registrationId"));
        address.setRegistrationCity(result.getString("registrationCity"));
        address.setRegistrationPlace(result.getString("registrationPlace"));
        address.setLiveId(ID.from(result, "liveId"));
        address.setLiveCity(result.getString("liveCity"));
        address.setLivePlace(result.getString("livePlace"));
        driver.setAddress(address);

        var job = new DriverJob();
        job.setId(ID.from(result, "jobId"));
        job.setPlace(result.getString("jobPlace"));
        job.setPost(result.getString("jobPost"));
        driver.setJob(job);
        
        photo.setId(ID.from(result, "photoId"));
        photoPath = result.getString("photoPath");
                
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        try(var inputStream = new FileInputStream(photoPath);
            var bufferedStream = new BufferedInputStream(inputStream)) {
            var packet = new byte[4096];
            int length;
            
            while((length = bufferedStream.read(packet)) > 0) {
                byteStream.write(packet, 0, length);
            }
        }
        
        photo.setImage(byteStream.toByteArray());
        photo.setLength(byteStream.size());
        driver.setPhoto(photo);

        return driver;
    }

    public void save(Driver driver) throws Exception {
        try (var connection = getConnection();
             var statement = connection.createStatement()) {
            if (driver.getId() == null) {
                insertDriver(statement, driver);
            } else {
                updateDriver(statement, driver);
            }
            savePhoto(statement, driver.getId(), driver.getPhoto());
        }
    }
    
    private void insertDriver(Statement statement, Driver driver) throws SQLException {
        driver.setId(ID.next());
        statement.execute(String.format(INSERT_DRIVER, driver.getId(), driver.getName(), driver.getSurname(), driver.getPatronymic()));
        
        var passport = driver.getPassport();
        passport.setId(ID.next());
        statement.execute(String.format(INSERT_PASSPORT, passport.getId(), driver.getId(), passport.getNumber(), passport.getSeries()));
        
        var contacts = driver.getContacts();
        contacts.setId(ID.next());
        statement.execute(String.format(INSERT_CONTACTS, contacts.getId(), driver.getId(), contacts.getPhone(), contacts.getEmail()));
        
        var address = driver.getAddress();
        address.setRegistrationId(ID.next());
        statement.execute(String.format(INSERT_ADDRESS, address.getRegistrationId(), driver.getId(), address.getRegistrationCity(), address.getRegistrationPlace()));
        address.setLiveId(ID.next());
        statement.execute(String.format(INSERT_ADDRESS, address.getLiveId(), driver.getId(), address.getLiveCity(), address.getLivePlace()));
        
        var job = driver.getJob();
        statement.execute(String.format(INSERT_JOB, job.getId(), driver.getId(), job.getPlace(), job.getPost(), job.getId()));      
    }

    private void updateDriver(Statement statement, Driver driver) throws SQLException {
        statement.execute(String.format(UPDATE_DRIVER, driver.getName(), driver.getSurname(), driver.getPatronymic(), driver.getId()));
        
        var passport = driver.getPassport();
        statement.execute(String.format(UPDATE_PASSPORT, passport.getNumber(), passport.getSeries(), passport.getId()));
        
        var contacts = driver.getContacts();
        statement.execute(String.format(UPDATE_CONTACTS, contacts.getPhone(), contacts.getEmail(), contacts.getId()));
        
        var address = driver.getAddress();
        statement.execute(String.format(UPDATE_ADDRESS, address.getRegistrationCity(), address.getRegistrationPlace(), address.getRegistrationId()));
        statement.execute(String.format(UPDATE_ADDRESS, address.getLiveCity(), address.getLivePlace(), address.getLiveId()));
        
        var job = driver.getJob();
        statement.execute(String.format(UPDATE_JOB, job.getPlace(), job.getPost(), job.getId()));        
    }
    
    private void savePhoto(Statement statement, UUID driverId, Photo photo) throws SQLException, IOException {
        var path = "~/.drivers/" + driverId + "/photo.img";
        if (photo.getId() == null) {
            statement.execute(String.format(INSERT_PHOTO, photo.getId(), driverId, path));
        }
        
        var file = new File(path);
        if (file.exists()) {
            file.delete();
        } else {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        
        try(var outputStream = new FileOutputStream(path);
            var bufferedStream = new BufferedOutputStream(outputStream)) {
            bufferedStream.write(photo.getImage());
            bufferedStream.flush();
        }
    }

}
