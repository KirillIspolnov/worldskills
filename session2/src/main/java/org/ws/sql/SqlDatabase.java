package org.ws.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlDatabase {
    private String url;

    public SqlDatabase(String url) {
        this.url = url;
    }

    protected Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url);
    }

    protected boolean execute(String sql) throws SQLException {
        try(Connection connection = getConnection();
            Statement statement = connection.createStatement()) {
            return statement.execute(sql);
        }
    }
}
