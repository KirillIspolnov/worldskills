package org.ws.sql;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;

import org.ws.abstraction.AuthRepository;
import org.ws.models.Session;
import org.ws.models.Session.Status;
import org.ws.models.User;
import org.ws.utils.ID;

public class SqlAuthRepository extends SqlDatabase implements AuthRepository {

    public SqlAuthRepository(String url) {
        super(url);
    }
    
    @Override
    public void init() throws Exception {
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
            execute("CREATE TABLE IF NOT EXISTS users (id uuid, login text, hash text, PRIMARY KEY(id, login))");
            execute("CREATE TABLE IF NOT EXISTS sessions (id uuid PRIMARY KEY, user uuid REFERENCES users(id), status int, created date, attempts int)");
        } catch (SQLException e) {
            System.err.println(e);
        }
    }
    
    private static final String SELECT_USER_BY_LOGIN = """
            SELECT * FROM users WHERE login='%s'
            """;
    
    private static final String SELECT_SESSION_BY_USER = """
            SELECT * FROM sessions WHERE user='%s'
            """;
    
    private static final String INSERT_SESSION = """
            INSERT INTO sessions (id, user, status, create) VALUES ('%s', '%s', %d, '%s')
            """;
    
    private static final String UPDATE_SESSION = """
            UPDATE sessions SET (status = %d) WHERE id='%s'
            """;

    @Override
    public User getUser(String login) throws Exception {
        try (var connection = getConnection();
             var statement = connection.createStatement()) {
             var result = statement.executeQuery(String.format(SELECT_USER_BY_LOGIN, login));
             
             if (!result.next()) {
                 return null;
             }
             
             var user = new User();
             user.setId(ID.from(result, "id"));
             user.setLogin(result.getString("login"));
             user.setPasswordHash(result.getString("passwordHash"));
             return user;
        }
    }

    @Override
    public Session getUserSession(User user) throws Exception {
        try (var connection = getConnection();
             var statement = connection.createStatement()) {
             var result = statement.executeQuery(String.format(SELECT_SESSION_BY_USER, user.getId()));
             
             var session = new Session();
             session.setId(ID.from("id"));
             session.setUser(user);
             session.setDate(result.getDate("created"));
             
             session.setStatus(switch(result.getInt("status")) {
                 case 0 -> Status.FAILED;
                 case 1 -> Status.SUCCESS;
                 case 2 -> Status.BLOCKED;
                 default -> null;
             });
             
             return session;
        }
    }

    @Override
    public void saveSession(Session session) throws Exception {
        try (var connection = getConnection();
            var statement = connection.createStatement()) {
            
            var status = switch(session.getStatus()) {
                case FAILED -> 0;
                case SUCCESS -> 1;
                case BLOCKED -> 2;
            };
            
            if (session.getId() == null) {
                session.setId(ID.next());
                session.setDate(new Date(System.currentTimeMillis()));
                
                statement.execute(String.format(INSERT_SESSION, session.getId(), session.getUser().getId(), status, session.getDate()));
            } else {
                statement.execute(String.format(UPDATE_SESSION, status, session.getId()));
            }
       }
    }


}
