package org.ws.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ID {
    public static UUID next() {
        return UUID.randomUUID();
    }

    public static UUID from(String id) {
        return UUID.fromString(id);
    }

    public static UUID from(ResultSet sql, String column) throws SQLException {
        return from(sql.getString(column));
    }
}
