package org.ws.utils;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

public class UI {
    public static void invokeTask(Runnable task) {
        try {
            SwingUtilities.invokeAndWait(task);
        } catch (InvocationTargetException | InterruptedException e) {
             e.printStackTrace();
        }
    }
    
    public static void invokeTask(Runnable task, long delay) {
        var thread = new Thread(() -> {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            
            invokeTask(task);
         });
         thread.start();
    }
}
